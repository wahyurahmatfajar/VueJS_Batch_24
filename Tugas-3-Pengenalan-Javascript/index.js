/* 
SOAL 1

buatlah variabel-variabel seperti di bawah ini

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
gabungkan variabel-variabel tersebut agar menghasilkan output

saya senang belajar JAVASCRIPT 
*/

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var ketiga = pertama.substr(0, 4);

var keempat = pertama.substr(12, 6);

var kelima = ketiga + " " + keempat;

var keenam = kedua.substr(0, 7);

var ketujuh = kedua.substr(8, 10);

var upper = ketujuh.toUpperCase();

var kedelapan = keenam + " " + upper;

var kalimat = kelima + " " + kedelapan;

console.log(kalimat);

/* 
soal 2

buatlah variabel-variabel seperti di bawah ini

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
ubahlah variabel diatas ke dalam integer dan lakukan operasi matematika semua variabel agar menghasilkan output 24 (integer).
*catatan :
1. gunakan 3 operasi, tidak boleh  lebih atau kurang. contoh : 10 + 2 * 4 + 6
2. Boleh menggunakan tanda kurung . contoh : (10 + 2 ) * (4 + 6) */

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var intPertama = parseInt(kataPertama);
var intKedua = parseInt(kataKedua);
var intKetiga = parseInt(kataKetiga);
var intKeempat = parseInt(kataKeempat);

var jumlah = (intPertama + intKeempat) + (intKedua * intKetiga);

console.log(jumlah);

/* 
soal 3

buatlah variabel-variabel seperti di bawah ini

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua; // do your own! 
var kataKetiga; // do your own! 
var kataKeempat; // do your own! 
var kataKelima; // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
selesaikan variabel yang belum diisi dan hasilkan output seperti berikut:

Kata Pertama: wah
Kata Kedua: javascript
Kata Ketiga: itu
Kata Keempat: keren
Kata Kelima: sekali */

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24); 
var kataKelima = kalimat.substring(25, 31); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

/* 1.
Judul : Function Penghasil Tanggal Hari Esok

Buatlah sebuah function dengan nama next_date() yang menerima 3 parameter tanggal, bulan, tahun dan mengembalikan nilai tanggal hari esok dalam bentuk string, dengan contoh input dan otput sebagai berikut.

contoh 1

var tanggal = 29
var bulan = 2
var tahun = 2020

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020

contoh 2

var tanggal = 28
var bulan = 2
var tahun = 2021

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021

contoh 3

var tanggal = 31
var bulan = 12
var tahun = 2020

next_date(tanggal , bulan , tahun ) // output : 1 Januari 2021 */

var tanggal = 31;
var bulan = 12;
var tahun = 2020;



function next_date(tgl, bln, thn) {

    if(tgl > 1){
        tgl = 1;
    }

    if(bln > 1){
        bln += 1;
    }

    if(bln == 13){
        if(thn >= tahun){
            thn += 1;
        }
        bln = 1;
    }


    switch(bln) {
        case 1: {
            bln = 'Januari';
        break;
        }
        case 2: {
            bln = 'Februari';
        break;
        }
        case 3: {
            bln = 'Maret';
        break;
        }
        case 4: {
            bln = 'April';
        break;
        }
        case 5: {
            bln = 'Mei';
        break;
        }
        case 6: {
            bln = 'Juni';
        break;
        }
        case 7: {
            bln = 'Juli';
        break;
        }
        case 8: {
            bln = 'Agustus';
        break;
        }
        case 9: {
            bln = 'September';
        break;
        }
        case 10: {
            bln = 'Oktober';
        break;
        }
        case 11: {
            bln = 'November';
        break;
        }
        case 12: {
            bln = 'Desember';
        break;
        }
        default : {
            bln = 'Bukan nama bulan';
        }
    }
    
    tanggals = tgl + " " + bln + " " + thn;

}

next_date(tanggal , bulan , tahun );

console.log(tanggals);

/* 2.
Judul : Function Penghitung Jumlah Kata

Buatlah sebuah function dengan nama jumlah_kata() yang menerima sebuah kalimat (string), dan mengembalikan nilai jumlah kata dalam kalimat tersebut.

Contoh

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = "Saya Iqbal"


jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2 */

function jumlah_kata(item) {
    return item.split(" ").length;
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = "Saya Iqbal";

jumlah_kata(kalimat_1);
jumlah_kata(kalimat_2);

console.log(jumlah_kata(kalimat_1), jumlah_kata(kalimat_2));

export const Smartphone = {
    data () {
        return {
            smartphones: [
                {
                    id: 1,
                    title: 'Xiaomi'
                },
                {
                    id: 2,
                    title: 'Samsung'
                },
                {
                    id: 3,
                    title: 'Oppo'
                },
                {
                    id: 4,
                    title: 'Realme'
                },
                {
                    id: 5,
                    title: 'Vivo'
                },
                
            ]
        }
    },
    template: `
        <div>
            <h1>Daftar Gadget</h1>
            <ul>
                <li v-for="smartphone of smartphones">
                    <router-link :to="'/smartphone/'+smartphone.id"> 
                        {{ smartphone.title }} 
                    </router-link>
                </li>
            </ul>
        </div>
    ` 
}
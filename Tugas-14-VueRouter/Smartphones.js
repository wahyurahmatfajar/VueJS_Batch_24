export const Smartphones = {
    data () {
        return {
            smartphones: [
                {
                    id: 1,
                    title: 'Xiaomi',
                    harga: 'Rp. 3.000.000 - Rp. 10.000.000'
                },
                {
                    id: 2,
                    title: 'Samsung',
                    harga: 'Rp. 3.000.000 - Rp. 10.000.000'
                },
                {
                    id: 3,
                    title: 'Oppo',
                    harga: 'Rp. 3.000.000 - Rp. 10.000.000'
                },
                {
                    id: 4,
                    title: 'Realme',
                    harga: 'Rp. 3.000.000 - Rp. 10.000.000'
                },
                {
                    id: 5,
                    title: 'Vivo',
                    harga: 'Rp. 3.000.000 - Rp. 10.000.000'
                },
                
            ]
        }
    },
    computed: {
        smartphone() {
            return this.smartphones.filter((smartphone)=>{
                return smartphone.id === parseInt(this.$route.params.id)                
            })[0]
        }
    },
    template: `
        <div id="listhp">
            <h1>Smart Phone :  {{ smartphone.title }}</h1>
            <ul>
                <li v-for="(num, value) of smartphone">
                    {{ value +' : '+ num }} <br>
                </li>
            </ul>
        </div>`, 
}
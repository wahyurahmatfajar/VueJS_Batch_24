export const Laptops = {
    data () {
        return {
            laptops: [
                {
                    id: 1,
                    title: 'Asus',
                    harga: 'Rp. 5.000.000 - Rp. 100.000.000'
                },
                {
                    id: 2,
                    title: 'HP',
                    harga: 'Rp. 5.000.000 - Rp. 100.000.000'
                },
                {
                    id: 3,
                    title: 'Acer',
                    harga: 'Rp. 5.000.000 - Rp. 100.000.000'
                },
                {
                    id: 4,
                    title: 'Lenovo',
                    harga: 'Rp. 5.000.000 - Rp. 100.000.000'
                },
                {
                    id: 5,
                    title: 'Dell',
                    harga: 'Rp. 5.000.000 - Rp. 100.000.000'
                },
                
            ]
        }
    },
    computed: {
        laptop() {
            return this.laptops.filter((laptop)=>{
                return laptop.id === parseInt(this.$route.params.id)                
            })[0]
        }
    },
    template: `
        <div>
            <h1>Laptop :  {{ laptop.title }}</h1>
            <ul>
                <li v-for="(num, value) of laptop">
                    {{ value +' : '+ num }} <br>
                </li>
            </ul>
        </div>`, 
}
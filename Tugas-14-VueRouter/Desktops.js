export const Desktops = {
    data () {
        return {
            desktops: [
                {
                    id: 1,
                    title: 'Asus',
                    harga: 'Rp. 5.000.000 - Rp. 100.000.000'
                },
                {
                    id: 2,
                    title: 'HP',
                    harga: 'Rp. 5.000.000 - Rp. 100.000.000'
                },
                {
                    id: 3,
                    title: 'Acer',
                    harga: 'Rp. 5.000.000 - Rp. 100.000.000'
                },
                {
                    id: 4,
                    title: 'Lenovo',
                    harga: 'Rp. 5.000.000 - Rp. 100.000.000'
                },
                {
                    id: 5,
                    title: 'Dell',
                    harga: 'Rp. 5.000.000 - Rp. 100.000.000'
                },
                
            ]
        }
    },
    computed: {
        desktop() {
            return this.desktops.filter((desktop)=>{
                return desktop.id === parseInt(this.$route.params.id)                
            })[0]
        }
    },
    template: `
        <div>
            <h1>PC Desktop :  {{ desktop.title }}</h1>
            <ul>
                <li v-for="(num, value) of desktop">
                    {{ value +' : '+ num }} <br>
                </li>
            </ul>
        </div>`, 
}
export const Laptop = {
    data () {
        return {
            laptops: [
                {
                    id: 1,
                    title: 'Asus'
                },
                {
                    id: 2,
                    title: 'HP'
                },
                {
                    id: 3,
                    title: 'Acer'
                },
                {
                    id: 4,
                    title: 'Lenovo'
                },
                {
                    id: 5,
                    title: 'Dell'
                },
                
            ]
        }
    },
    template: `
        <div>
            <h1>Daftar Laptop</h1>
            <ul>
                <li v-for="laptop of laptops">
                    <router-link :to="'/laptop/'+laptop.id"> 
                        {{ laptop.title }} 
                    </router-link>
                </li>
            </ul>
        </div>
    ` 
}
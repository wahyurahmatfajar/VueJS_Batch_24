export const Desktop= {
    data () {
        return {
            desktops: [
                {
                    id: 1,
                    title: 'Asus'
                },
                {
                    id: 2,
                    title: 'HP'
                },
                {
                    id: 3,
                    title: 'Acer'
                },
                {
                    id: 4,
                    title: 'Lenovo'
                },
                {
                    id: 5,
                    title: 'Dell'
                },
                
            ]
        }
    },
    template: `
        <div>
            <h1>Daftar PC Desktop</h1>
            <ul>
                <li v-for="desktop of desktops">
                    <router-link :to="'/desktop/'+desktop.id"> 
                        {{ desktop.title }} 
                    </router-link>
                </li>
            </ul>
        </div>
    ` 
}
/* 
soal 1
buatlah variabel seperti di bawah ini

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):

1. Tokek
2. Komodo
3. Cicak
4. Ular
5. Buaya */

console.log('Soal 1');
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort();

daftarHewan.forEach(function(item){
    console.log(item);
});
console.log('\n');

/* 
soal 2
Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"

/* 
    Tulis kode function di sini
*/
 
/* var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan)  */// Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"  */

console.log('Soal 2');

function introduce(array) {
    return `Nama saya ` + array.name + `, umur saya ` + array.age + ` tahun, alamat saya di ` + array.address + `, dan saya punya hobby yaitu ` + array.hobby + `!`;
};

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };
var perkenalan = introduce(data);
console.log(perkenalan);

console.log('\n');

/* soal 3
Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) */ // 3 2

console.log('Soal 3');

function hitung_huruf_vokal(item) {
    
    vokal = ['a', 'e', 'i', 'o', 'u','A', 'E', 'I', 'O', 'U'];
    jumlahVokal = 0;

    for (i = 0; i < item.length; i++) {
		for (j = 0; j < vokal.length; j++) {
			if (item[i] === vokal[j]) {
				jumlahVokal++;
			}
		}
	}

    return jumlahVokal;

}

var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1 , hitung_2);

console.log('\n');

/* 
soal 4

Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8 */


console.log('Soal 4');

function hitung(angka) {
    return angka = (angka * 2) - 2;
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8

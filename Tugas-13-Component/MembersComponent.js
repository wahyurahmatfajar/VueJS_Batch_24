export const MembersComponent = {
    template : `
        <tr>
            <td>
                <slot></slot>
            </td>
            <td>
                <b> Nama : </b> {{ member.name }}  <br>
                <b> Alamat : </b> {{ member.address }}  <br>
                <b> No Handphone : </b> {{ member.no_hp }}  <br>
            </td>
            <td>
                <button @click="$emit('edit', 'member')">Edit</button>
                <button @click="$emit('hapus', 'member.id')">Hapus</button>
                <button @click="$emit('upload', 'member')">Upload Photo</button>
            </td>
        </tr>`,

    props : ['member', 'photoDomain']
}
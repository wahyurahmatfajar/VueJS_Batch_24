/* 
soal 1

buatlah variabel seperti di bawah ini

var nilai;
pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi

nilai >= 85 indeksnya A
nilai >= 75 dan nilai < 85 indeksnya B
nilai >= 65 dan nilai < 75 indeksnya c
nilai >= 55 dan nilai < 65 indeksnya D
nilai < 55 indeksnya E */

var nilai = 68;

if(nilai > 85){
    console.log('A')
} else if(nilai >= 75 && nilai < 85) {
    console.log('B')
} else if(nilai >= 65 && nilai < 75) {
    console.log('C')
} else if(nilai >= 55 && nilai < 65) {
    console.log('D')
} else {
    console.log('E')
};

/* 
soal 2

buatlah variabel seperti di bawah ini

var tanggal = 22;
var bulan = 7;
var tahun = 2020;
ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing)
 */

var tanggal = 20;
var bulan = 8;
var tahun = 1998;

switch(bulan) {
    case 1: {
        bulan = 'Januari';
    break;
    }
    case 2: {
        bulan = 'Februari';
    break;
    }
    case 3: {
        bulan = 'Maret';
    break;
    }
    case 4: {
        bulan = 'April';
    break;
    }
    case 5: {
        bulan = 'Mei';
    break;
    }
    case 6: {
        bulan = 'Juni';
    break;
    }
    case 7: {
        bulan = 'Juli';
    break;
    }
    case 8: {
        bulan = 'Agustus';
    break;
    }
    case 9: {
        bulan = 'September';
    break;
    }
    case 10: {
        bulan = 'Oktober';
    break;
    }
    case 11: {
        bulan = 'November';
    break;
    }
    case 12: {
        bulan = 'Desember';
    break;
    }
    default : {
        bulan = 'Bukan nama bulan';
    }
}

var ttl = tanggal + ' ' + bulan + ' ' + tahun;

console.log(ttl);

/* 
soal 3
Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n. Looping boleh menggunakan syntax apa pun (while, for, do while).

Output untuk n=3 :

#
##
###
Output untuk n=7 :

#
##
###
####
#####
######
####### */

var tagar = "";

for(var i = 10; i > 0; i -= 1) {
    for(var j = i; j <= 10; j++) {
        tagar += '#';
    }
    tagar += '\n';
};

console.log(tagar);



/* 
soal 4
berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m, dan berikan output sebagai berikut.
contoh :

Output untuk m = 3

1 - I love programming
2 - I love Javascript
3 - I love VueJS
===

Output untuk m = 5

1 - I love programming
2 - I love Javascript
3 - I love VueJS
===
4 - I love programming
5 - I love Javascript

Output untuk m = 7

1 - I love programming
2 - I love Javascript
3 - I love VueJS
===
4 - I love programming
5 - I love Javascript
6 - I love VueJS
======
7 - I love programming


Output untuk m = 10

1 - I love programming
2 - I love Javascript
3 - I love VueJS
===
4 - I love programming
5 - I love Javascript
6 - I love VueJS
======
7 - I love programming
8 - I love Javascript
9 - I love VueJS
=========
10 - I love programming */

output = 10;
tampung = "";

for(i = 1; i <= output; i++) {
    /* if(i%2==1)
    {
        tampung += i + " - I love Javascript \n";
    }
    else if(i%3 == 0) 
    {
        tampung += i + " - I love VueJS \n";
        for(j = i; j <output; j++) {
            tampung += '===';
            tampung += '\n';
        }
    } 
    else 
    {
        tampung += i + " - I love programming \n";
    } */

    if(i%2 == 1) {
        if(i%3 == 0) {
            tampung += i + " - I love VueJS \n";
            for(j=i; j<=3; j++){
                tampung += '===';
                tampung += '\n';
            }
        } else {
            tampung += i + " - I love programming \n";
        }
    } else if(i%3 == 0) {
        tampung += i + " - I love VueJS \n";
        for(j=i; j<=6; j++){
            tampung += '======';
            tampung += '\n';
        }
    } else {
        tampung += i + " - I love Javascript \n";
    }
}

console.log(tampung);